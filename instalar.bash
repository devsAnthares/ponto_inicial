#!/bin/bash

sed -i 's/\,/;/g' /opt/Ponto-Inicial/installProg.bash
sed -i 's/\[/ /g' /opt/Ponto-Inicial/installProg.bash
sed -i 's/\]/ /g' /opt/Ponto-Inicial/installProg.bash

xterm -e pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY bash /opt/Ponto-Inicial/installProg.bash
