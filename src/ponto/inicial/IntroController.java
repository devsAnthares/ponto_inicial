/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ponto.inicial;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import javafx.scene.control.Button;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class IntroController implements Initializable {
    
    @FXML private Button btn_sim;
    @FXML private Button btn_nao;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    void avancar(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Termo.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(IntroController.class.getName()).log(Level.SEVERE, null, ex);
        }
                Stage stage = new Stage();
                Scene scene = new Scene(tela);
                
                stage.initStyle(StageStyle.UNDECORATED);
                
                stage.setScene(scene);
                stage.show();
                
                stage = (Stage) btn_sim.getScene().getWindow();
                stage.close();
    }
    
    @FXML
    void fecharTela() throws IOException{
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Fechar.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(IntroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Stage stage = new Stage();
        Scene scene = new Scene(tela);
        stage.setScene(scene);
        stage.show();
    }
}
