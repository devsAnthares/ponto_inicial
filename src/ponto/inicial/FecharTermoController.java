/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ponto.inicial;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class FecharTermoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML private Button btn_voltar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    void cancelar(){
        Stage stage = (Stage) btn_voltar.getScene().getWindow(); //Obtendo a janela atual
        stage.close(); //Fechando o Stage
                
    }
    
    @FXML
    void fecharPrograma() throws IOException{
        
        Runtime r = Runtime.getRuntime();
        Process p = r.exec(new String[]{"/bin/sh", "-c", "bash /opt/Ponto-Inicial/desinstalarTotal.bash"   });
        Scanner scanner = new Scanner(p.getInputStream());
        String resultado = scanner.next();
        
        System.exit(0);
    }
    
}
